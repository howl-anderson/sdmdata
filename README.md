SDMdata
===

SDMdata is a web-based GBIF (Global Biodiversity Information Facility) data collect tools.

## Install
see [online install guide](document/setup/install.md) or [pdf version](document/setup/install.pdf)

## How to use
see [silde pdf](document/slide/introduce_sdmdata.pdf)

## For windows user
SDMdata currently not support windows. If wndows user want to use SDMdata tools, using virutal machine maybe is a good choice.

## More
If you have any question or suggestion, please send E-mail to us.

#!/usr/bin/env python

from datetime import timedelta
import yaml
import os

fp = open(os.path.join(os.path.dirname(os.path.realpath(__file__)), '../configure/configure.yaml'))
conf = yaml.load(fp)
fp.close()

DEBUG = conf['DEBUG']
SECRET_KEY = conf['SECRET_KEY']

PERMANENT_SESSION_LIFETIME = timedelta(minutes=30)
REMEMBER_COOKIE_DURATION = timedelta(days=30)

DATABASE_URI = conf['DATABASE_HOST_URI'] + conf['DATABASE_DATA_URI']
DATABASE_HOST_URI = conf['DATABASE_HOST_URI']

MAIL_SERVER = 'smtp.qq.com'
MAIL_PORT = 465
MAIL_USE_SSL = True
MAIL_DEBUG = True
MAIL_USERNAME = '359961535@qq.com'
MAIL_PASSWORD = '8q9q0Q1Q0101#@'

# MAIL_SERVER = 'smtp.googlemail.com'
# MAIL_PORT = 465
# MAIL_USE_TLS = False
# MAIL_USE_SSL = True
# MAIL_USERNAME = 'u1mail2me@gmail.com'
# MAIL_PASSWORD = 'Df5Pvw6Hq4lnb3C0'

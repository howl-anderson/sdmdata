#!/usr/bin/env python
# -*- coding:utf-8 -*-

from flask import Blueprint, render_template, abort
import os
from jinja2 import TemplateNotFound

from flask_mail import Message
from flask_mail import Mail

current_dir = os.path.dirname(os.path.abspath(os.path.relpath(__file__)))
template_folder = os.path.join(current_dir, 'templates')

user_app = Blueprint('user', __name__, template_folder=template_folder)
mail = Mail()


@user_app.record_once
def on_load(state):
    mail.init_app(state.app)


def send_mail():
    msg = Message("Hello", sender="359961535@qq.com", recipients=["359961535@qq.com"])
    msg.body = "testing"
    msg.html = "<b>大家好</b>"
    mail.send(msg)


@user_app.route('/register')
def register():
    return render_template('register.html')